import React from 'react'

export default function Place({ shelf, slot, data, active }) {

  const renderSlotItems = () => {
    if (data) {
      let items = [];

      for (let i = 0; i < data.count; i++) {
        items.push(
          <div key={i} className="slot__item" style={{transform: `translate(${(data.count - i) * 5}px, -${(data.count - i) * 5}px)`}}>
            <img src={data.image} alt={data.name} />
          </div>
        );
      }

      return items;
    }

    return null;
  }

  return (
    <div className={`slot ${active ? 'active' : ''}`}>
      {renderSlotItems()}
      <div className="slot-id">{`${shelf}${slot}`}</div>
    </div>
  );
}