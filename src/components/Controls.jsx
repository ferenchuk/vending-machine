import React from 'react';

export default function Controls(props) {
  const {
    balance,
    onChange,
    onCancelSelection,
    onConfirmSelection,
    onMoneyReturn,
    disabled
  } = props;

  const onNumClick = num => {
    onChange(num)
  }

  return (
    <div className="controls">
      <div className="controls__display">
        {balance}
      </div>

      <button className="controls__clear" onClick={onMoneyReturn}>C</button>

      <div className="controls__buttons">
        <div className="button-wrap"><button onClick={() => onNumClick(1)} disabled={disabled}>1</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(2)} disabled={disabled}>2</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(3)} disabled={disabled}>3</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(4)} disabled={disabled}>4</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(5)} disabled={disabled}>5</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(6)} disabled={disabled}>6</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(7)} disabled={disabled}>7</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(8)} disabled={disabled}>8</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(9)} disabled={disabled}>9</button></div>
        <div className="button-wrap"><button onClick={onCancelSelection} disabled={disabled}>C</button></div>
        <div className="button-wrap"><button onClick={() => onNumClick(0)} disabled={disabled}>0</button></div>
        <div className="button-wrap"><button onClick={onConfirmSelection} disabled={disabled}>OK</button></div>
      </div>
    </div>
  )
}
