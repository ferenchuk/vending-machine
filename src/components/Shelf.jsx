import React from 'react';
import Place from './Place';

export default function Shelf({ items, shelfIndex, activeSelection }) {
  const selectedShelf = activeSelection.shelf === shelfIndex + 1;

  return (
    <div className="shelf">
      {items.map((slot, slotIndex) => (
        <Place
          slot={slotIndex + 1}
          shelf={shelfIndex + 1}
          data={slot}
          key={slotIndex}
          active={selectedShelf && slotIndex + 1 === activeSelection.place}
        />
      ))}
    </div>
  );
}