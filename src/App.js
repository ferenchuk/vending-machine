import React, { useState, useRef, useEffect } from 'react';
import './App.scss';
import Controls from './components/Controls';
import Shelf from './components/Shelf';
import shelvesArray from './items';

const defaultCoins = [
  { value: 200, count: 0 },
  { value: 100, count: 0 },
  { value: 50, count: 0 },
  { value: 20, count: 0 },
  { value: 10, count: 0 },
  { value: 5, count: 0 }
];

function App() {
  const ref = useRef(null);

  const [shelves, setShelves] = useState(shelvesArray);
  const [coins, setCoins] = useState(defaultCoins);
  const [balance, setBalance] = useState(0);
  const [isBalanceUpdating, setIsBalanceUpdating] = useState(false);
  const [showDroppedItem, setShowDroppedItem] = useState(false);
  const [activeSelection, setActiveSelection] = useState({
    shelf: 0,
    place: 0
  });

  useEffect(() => {
    if (!isBalanceUpdating) {
      getChange(balance);
    }
  }, [isBalanceUpdating, balance])

  const onNumSelect = (key) => {
    const { shelf, place } = activeSelection;

    if (shelf === 0 && key <= shelves.length) {
      return setActiveSelection({ shelf: key, place });
    }

    if (place === 0 && key <= shelves[shelf - 1]?.length) {
      return setActiveSelection({ shelf, place: key });
    }

    return null;
  }

  const onCancelSelection = () => {
    setActiveSelection({ shelf: 0, place: 0 })
  }

  const onConfirmSelection = () => {
    if (activeSelection.shelf === 0 && activeSelection.place === 0) return;

    const currentItem = shelves[activeSelection.shelf -1][activeSelection.place -1];

    if (!currentItem || currentItem.count === 0) return;
    if (balance < currentItem.price) return;

    const newShelves = shelves.map(item => item);
    ref.current.classList.add('push');

    setTimeout(() => {
      newShelves[activeSelection.shelf -1][activeSelection.place -1].count -= 1;
      setShelves(newShelves);
      ref.current.classList.remove('push');
    }, 3000);

    setTimeout(() => setShowDroppedItem(true), 700);
    setTimeout(() => setShowDroppedItem(false), 4000);

    updateBalance(- currentItem.price);
  }

  const getChange = (amount) => {
    let calculatedBalance = amount;

    const coins = defaultCoins.map(coin => {
        let amountCoin = Math.floor(calculatedBalance / coin.value)
        calculatedBalance -= amountCoin * coin.value

        return {
          ...coin,
          count: amountCoin
        }
    });

    setCoins(coins);
  }

  const updateBalance = (value) => {
    if (value === 0) return;

    setIsBalanceUpdating(true);
    let start = balance;
    const end = balance + value;
    const increment = value > 0 ? 1 : -1;
    const duration = 1000;
    const stepTime = Math.abs(Math.floor(duration / value));
    const timer = setInterval(() => {
      start += increment
      setBalance(start);

      if (start === end) {
        clearInterval(timer);
        setIsBalanceUpdating(false);
      }
    }, stepTime);
  }

  const renderShelves = shelves.map((shelf, index) => (
    <Shelf
      key={index}
      items={shelf}
      shelfIndex={index}
      activeSelection={activeSelection}
    />
  ));
  
  return (
    <div className="App">
      <div className="wrap">
        <div className="vending-machine">
          <div ref={ref} className="vending-slots">
            {renderShelves}
          </div>
          <Controls
            balance={balance}
            onChange={onNumSelect}
            onCancelSelection={onCancelSelection}
            onMoneyReturn={() => updateBalance(-balance)}
            onConfirmSelection={onConfirmSelection}
            disabled={isBalanceUpdating}
          />

          <div className="vending-footer">
            {showDroppedItem && shelves[activeSelection.shelf -1][activeSelection.place -1] && (
              <img src={shelves[activeSelection.shelf -1][activeSelection.place -1].image} alt="" />
            )}
          </div>
        </div>

        <div className="coins">
          <button className="coin" onClick={() => updateBalance(50)} disabled={isBalanceUpdating}>50</button>
          <button className="coin" onClick={() => updateBalance(100)} disabled={isBalanceUpdating}>100</button>
          <button className="coin" onClick={() => updateBalance(200)} disabled={isBalanceUpdating}>200</button>

          <div className="coins-back">
            {coins.filter(item => item.count !== 0).map(item => (
              <div>
                <button key={item.value} className="coin">{item.value}</button>
                x {item.count}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
