import snickersImg from './assets/product-snickers.png';
import bombaImg from './assets/product-bomba.png';
import marsImg from './assets/product-mars.png';

const snickers = {
  name: 'snickers',
  image: snickersImg,
  price: 220,
  count: 3
};

const mars = {
  name: 'mars',
  image: marsImg,
  price: 190,
  count: 3
};

const bomba = {
  name: 'bomba',
  image: bombaImg,
  price: 315,
  count: 3
}

const items = [
  [{...snickers}, null, {...bomba}, null, null],
  [null, {...mars}, {...snickers}, null, null],
  [null, null, {...bomba}, {...snickers}, null],
  [null, {...mars}, null, null, null],
  [null, null, {...mars}, null, null]
];

export default items;
